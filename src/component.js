import {updateComponent} from './virtual-dom';

export default class Component {
  state = {};

  setState(stateChange) {
    this.state = {...this.state, ...stateChange};
    updateComponent(this);
  }

  componentDidMount() {}
}