export const render = (component, root) => {
  const virtualDom = component.render();
  component.virtualDom = virtualDom;
  component.root = root;

  root.appendChild(renderTree(virtualDom));
  component.componentDidMount();
};

export const updateComponent = component => {
  const newVirtualDom = component.render();
  diff(component.virtualDom, newVirtualDom, component.root.firstChild);
  component.virtualDom = newVirtualDom;
};

const renderTree = tree => {
  let element;
  if (typeof tree === 'string') {
    element = document.createTextNode(tree);
  } else {
    const {elementType, attrs, events, children} = tree;
    element = document.createElement(elementType);

    for (const attrName in attrs) {
      element.setAttribute(attrName, attrs[attrName]);
    }
    for (const eventName in events) {
      element.addEventListener(eventName, events[eventName]);
    }
    
    for (const child of children) {
      if (child) {
        element.appendChild(renderTree(child, element));
      }
    }
  }
  return element;
};

const diff = (oldTree, newTree, element) => {
  if (typeof newTree === 'string' && newTree !== oldTree || newTree.elementType !== oldTree.elementType) {
    element.replaceWith(renderTree(newTree));
  } else {
    for (const attrName in {...oldTree.attrs, ...newTree.attrs}) {
      diffAttr(attrName, oldTree.attrs[attrName], newTree.attrs[attrName], element);
    }
    for (const eventName in {...oldTree.events, ...newTree.events}) {
      diffEvent(eventName, oldTree.events[eventName], newTree.events[eventName], element);
    }

    const childrenLength = Math.max(oldTree.children.length, newTree.children.length);
    let currChildIndex = 0;

    for (let i = 0; i < childrenLength; i++) {
      const oldChildTree = oldTree.children[i];
      const newChildTree = newTree.children[i];
      const childElem = element.children[currChildIndex];

      if (oldChildTree && !newChildTree) {
        element.remove();
      } else {
        if (oldChildTree) {
          diff(oldChildTree, newChildTree, childElem);
        } else if (newChildTree) {
          insertChildAtIndex(element, renderTree(newChildTree), currChildIndex);
        }

        currChildIndex++;
      }
    }
  }
};

const diffAttr = (attrName, oldAttr, newAttr, element) => {
  if (newAttr) {
    if (oldAttr !== newAttr) {
      element.setAttribute(attrName, newAttr);
    }
  } else if (oldAttr) {
    element.removeAttribute(attrName);
  }
};

const diffEvent = (eventName, oldEvent, newEvent, element) => {
  if (oldEvent !== newEvent) {
    if (oldEvent) {
      element.removeEventListener(eventName, oldEvent); 
    }
    if (newEvent) {
      element.addEventListener(eventName, newEvent); 
    }
  }
};


const insertChildAtIndex = (element, child, index) => {
  if (index >= element.children.length) {
    element.appendChild(child);
  } else {
    element.insertBefore(child, element.children[index]);
  }
};